package com.itmo.simpleq.ui.add_user;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

public class AddUserVm extends ViewModel {
    private MutableLiveData<String> mText;

    public AddUserVm() {
        mText = new MutableLiveData<>();
        mText.setValue("This is add user model fragment");
    }

    public LiveData<String> getText() {
        return mText;
    }
}
