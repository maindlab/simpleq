package com.itmo.simpleq.ui.check_querry;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.itmo.simpleq.R;


import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

public class CheckQFragment extends Fragment {
    private CheckQVM checkQVM;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        checkQVM =  ViewModelProviders.of(this).get(CheckQVM.class);
        View root = inflater.inflate(R.layout.fragment_create_q, container, false);
        final TextView textView = root.findViewById(R.id.text_dashboard);
        checkQVM.getText().observe(this, new Observer<String>() {
            @Override
            public void onChanged(@Nullable String s) {
                textView.setText(s);
            }
        });
        return root;
    }
}
