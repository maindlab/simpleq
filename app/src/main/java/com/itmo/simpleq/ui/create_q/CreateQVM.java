package com.itmo.simpleq.ui.create_q;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

public class CreateQVM extends ViewModel {
    private MutableLiveData<String> mText;

    public CreateQVM() {
        mText = new MutableLiveData<>();
        mText.setValue("This is add user model fragment");
    }

    public LiveData<String> getText() {
        return mText;
    }
}
