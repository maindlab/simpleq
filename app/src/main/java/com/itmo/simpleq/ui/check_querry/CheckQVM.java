package com.itmo.simpleq.ui.check_querry;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

public class CheckQVM extends ViewModel {
    private MutableLiveData<String> mText;

    public CheckQVM() {
        mText = new MutableLiveData<>();
        mText.setValue("This is q model fragment");
    }

    public LiveData<String> getText() {
        return mText;
    }
}
