package com.itmo.simpleq.ui.add_user;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.itmo.simpleq.R;
import com.itmo.simpleq.ui.check_querry.CheckQVM;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

public class AddUserFragment extends Fragment {
    private AddUserVm addUserVm;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
       addUserVm =  ViewModelProviders.of(this).get(AddUserVm.class);
        View root = inflater.inflate(R.layout.fragment_create_q, container, false);
        final TextView textView = root.findViewById(R.id.text_dashboard);
        addUserVm.getText().observe(this, new Observer<String>() {
            @Override
            public void onChanged(@Nullable String s) {
                textView.setText(s);
            }
        });
        return root;
    }
}
