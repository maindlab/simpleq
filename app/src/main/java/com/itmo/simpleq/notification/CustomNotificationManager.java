package com.itmo.simpleq.notification;

import android.app.Activity;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.graphics.Color;

import com.itmo.simpleq.AddInQ;
import com.itmo.simpleq.AgreeWithQ;
import com.itmo.simpleq.DisagreeWithQ;
import com.itmo.simpleq.MainActivity;
import com.itmo.simpleq.R;

import androidx.core.app.NotificationCompat;
import androidx.core.app.NotificationManagerCompat;

public class CustomNotificationManager {
    public  static final int  NOTIFY_ID= 187;

    public void notify(Context c, String qName){
        NotificationManager notificationManager =
                (NotificationManager) c.getSystemService(Context.NOTIFICATION_SERVICE);

        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
            NotificationChannel channel = new NotificationChannel("SimpleQ", "Simple Q channel",
                    NotificationManager.IMPORTANCE_HIGH);
            channel.setDescription("Simple Q channel");
            channel.enableLights(true);
            channel.setLightColor(Color.RED);
            channel.enableVibration(false);
            notificationManager.createNotificationChannel(channel);
        }
        NotificationManagerCompat.from(c).notify(CustomNotificationManager.NOTIFY_ID, new CustomNotificationManager().getBuilder(c,"Hyieohered","SimpleQ").build());
    }
    private NotificationCompat.Builder getBuilder(Context c, String qName, String channel_ID) {
        Intent intent = new Intent(c, AddInQ.class);
        Intent yesI = new Intent(c, AgreeWithQ.class);
        Intent noI = new Intent(c, DisagreeWithQ.class);
        PendingIntent cIntent = PendingIntent.getActivity(c,0,intent,PendingIntent.FLAG_CANCEL_CURRENT);
        PendingIntent yesActivity = PendingIntent.getActivity(c, 0, yesI, PendingIntent.FLAG_CANCEL_CURRENT);
        PendingIntent noActivity = PendingIntent.getActivity(c, 0, noI, PendingIntent.FLAG_CANCEL_CURRENT);
        return new NotificationCompat.Builder(c,channel_ID).setSmallIcon(R.drawable.ic_check_q)
                .setContentTitle("Add in querry").setContentText("R u wanna add in "+qName)
                .setPriority(NotificationCompat.PRIORITY_DEFAULT).setContentIntent(cIntent)
                .addAction(R.drawable.ic_yes,"yes",yesActivity)
                .addAction(R.drawable.ic_no,"no",noActivity)
                .setLargeIcon(BitmapFactory.decodeResource(c.getResources(),R.drawable.ic_q1))
                .setAutoCancel(true);
    }
}
