package com.itmo.simpleq;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.NotificationManagerCompat;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.View;

import com.itmo.simpleq.notification.CustomNotificationManager;


public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        new CustomNotificationManager().notify(this,"Ochered");
    }
    public void onClickLogin(View v){
        Intent intent =  new Intent(this,MainSimpleQ.class);
        startActivity(intent);

        this.finish();
    }
}
